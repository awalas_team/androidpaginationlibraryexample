package pl.awalas.paging.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pl.awalas.paging.RedditPost

@Database(entities = [(RedditPost::class)], version = 1, exportSchema = false)
abstract class RedditDatabase : RoomDatabase() {

    abstract fun posts(): RedditPostDao
}
