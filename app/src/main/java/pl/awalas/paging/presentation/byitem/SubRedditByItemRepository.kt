package pl.awalas.paging.presentation.byitem

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import android.support.annotation.MainThread
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi
import pl.awalas.paging.repository.RedditPostRepository
import java.util.concurrent.Executor

class SubRedditByItemRepository(
    private val redditApi: RedditApi,
    private val networkExecutor: Executor
) : RedditPostRepository {

    @MainThread
    override fun postsOfSubreddit(subReddit: String, pageSize: Int): LiveData<PagedList<RedditPost>> {
        val sourceFactory = SubRedditByItemDataSource(redditApi, subReddit)

        // We use toLiveData Kotlin ext. function here, you could also use LivePagedListBuilder
        return sourceFactory.toLiveData(
            // we use Config Kotlin ext. function here, could also use PagedList.Config.Builder
            config = PagedList.Config(
                pageSize = pageSize,
                enablePlaceholders = false,
                initialLoadSizeHint = pageSize * 2
            ),
            // provide custom executor for network requests, otherwise it will default to
            // Arch Components' IO pool which is also used for disk access
            fetchExecutor = networkExecutor)
    }
}

