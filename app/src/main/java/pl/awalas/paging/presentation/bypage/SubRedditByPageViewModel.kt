package pl.awalas.paging.presentation.bypage

import android.arch.lifecycle.ViewModel
import pl.awalas.paging.repository.RedditPostRepository

class SubRedditByPageViewModel(repository: RedditPostRepository) : ViewModel() {
    val posts = repository.postsOfSubreddit("androiddev", 30)

}

