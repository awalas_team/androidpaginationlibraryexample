package pl.awalas.paging.presentation.bypage

import android.arch.paging.PageKeyedDataSource
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class SubRedditByPageDataSource(
    private val redditApi: RedditApi,
    private val subredditName: String
) : PageKeyedDataSource<String, RedditPost>() {

    private var retry: (() -> Any)? = null

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, RedditPost>
    ) {}

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, RedditPost>) {
        redditApi.getTopAfter(
            subreddit = subredditName,
            after = params.key,
            limit = params.requestedLoadSize
        ).enqueue(
            object : retrofit2.Callback<RedditApi.ListingResponse> {
                override fun onFailure(call: Call<RedditApi.ListingResponse>, t: Throwable) {
                    retry = {
                        loadAfter(params, callback)
                    }
                }

                override fun onResponse(
                    call: Call<RedditApi.ListingResponse>,
                    response: Response<RedditApi.ListingResponse>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()?.data
                        val items = data?.children?.map { it.data } ?: emptyList()
                        retry = null
                        callback.onResult(items, data?.after)
                    } else {
                        retry = {
                            loadAfter(params, callback)
                        }
                    }
                }
            }
        )
    }

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, RedditPost>
    ) {
        val request = redditApi.getTop(
            subreddit = subredditName,
            limit = params.requestedLoadSize
        )
        // triggered by a refresh, we better execute sync
        try {
            val response = request.execute()
            val data = response.body()?.data
            val items = data?.children?.map { it.data } ?: emptyList()
            retry = null
            callback.onResult(items, data?.before, data?.after)
        } catch (ioException: IOException) {
            retry = {
                loadInitial(params, callback)
            }
        }
    }
}