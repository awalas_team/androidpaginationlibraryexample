package pl.awalas.paging.presentation.bypage

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi

class SubRedditByPageDataSourceFactory(
    private val redditApi: RedditApi,
    private val subredditName: String
) : DataSource.Factory<String, RedditPost>() {

    val sourceLiveData = MutableLiveData<SubRedditByPageDataSource>()

    override fun create(): DataSource<String, RedditPost> {
        val source = SubRedditByPageDataSource(redditApi, subredditName)
        sourceLiveData.postValue(source)
        return source
    }
}
