package pl.awalas.paging.presentation.byitem

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi

class SubRedditByItemDataSourceFactory(
    private val redditApi: RedditApi,
    private val subredditName: String
) : DataSource.Factory<String, RedditPost>() {

    val sourceLiveData = MutableLiveData<SubRedditByItemDataSource>()

    override fun create(): DataSource<String, RedditPost> {
        val source = SubRedditByItemDataSource(redditApi, subredditName)
        sourceLiveData.postValue(source)
        return source
    }
}