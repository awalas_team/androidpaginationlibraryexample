package pl.awalas.paging.presentation.byposition

import android.arch.lifecycle.ViewModel
import pl.awalas.paging.repository.RedditPostRepository

class SubRedditByPositionViewModel(repository: RedditPostRepository) : ViewModel() {
    val posts = repository.postsOfSubreddit("androiddev", 30)

}

