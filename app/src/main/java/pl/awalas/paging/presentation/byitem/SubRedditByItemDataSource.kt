package pl.awalas.paging.presentation.byitem

import android.arch.paging.ItemKeyedDataSource
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class SubRedditByItemDataSource(
    private val redditApi: RedditApi,
    private val subredditName: String
)
    : ItemKeyedDataSource<String, RedditPost>() {

    private var retry: (() -> Any)? = null

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<RedditPost>) {
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<RedditPost>) {
        redditApi.getTopAfter(subreddit = subredditName,
            after = params.key,
            limit = params.requestedLoadSize).enqueue(
            object : retrofit2.Callback<RedditApi.ListingResponse> {
                override fun onFailure(call: Call<RedditApi.ListingResponse>, t: Throwable) {
                    retry = {
                        loadAfter(params, callback)
                    }
                }

                override fun onResponse(
                    call: Call<RedditApi.ListingResponse>,
                    response: Response<RedditApi.ListingResponse>
                ) {
                    if (response.isSuccessful) {
                        val items = response.body()?.data?.children?.map { it.data } ?: emptyList()
                        retry = null
                        callback.onResult(items)
                    } else {
                        retry = {
                            loadAfter(params, callback)
                        }
                    }
                }
            }
        )
    }

    override fun getKey(item: RedditPost): String = item.name

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<RedditPost>) {
        val request = redditApi.getTop(
            subreddit = subredditName,
            limit = params.requestedLoadSize
        )

        try {
            val response = request.execute()
            val items = response.body()?.data?.children?.map { it.data } ?: emptyList()
            retry = null
            callback.onResult(items)
        } catch (ioException: IOException) {
            retry = {
                loadInitial(params, callback)
            }
        }
    }
}