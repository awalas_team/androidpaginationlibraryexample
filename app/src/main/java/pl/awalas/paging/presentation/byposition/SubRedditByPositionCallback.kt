package pl.awalas.paging.presentation.byposition

import android.arch.paging.PagedList
import android.support.annotation.MainThread
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

class SubredditBoundaryCallback(
    private val subredditName: String,
    private val webservice: RedditApi,
    private val handleResponse: (String, RedditApi.ListingResponse?) -> Unit,
    private val ioExecutor: Executor,
    private val networkPageSize: Int)
    : PagedList.BoundaryCallback<RedditPost>() {

    val networkState = helper.createStatusLiveData()

    /**
     * Database returned 0 items. We should query the backend for more items.
     */
    @MainThread
    override fun onZeroItemsLoaded() {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            webservice.getTop(
                subreddit = subredditName,
                limit = networkPageSize)
                .enqueue(createWebserviceCallback(it))
        }
    }

    /**
     * User reached to the end of the list.
     */
    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: RedditPost) {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            webservice.getTopAfter(
                subreddit = subredditName,
                after = itemAtEnd.name,
                limit = networkPageSize)
                .enqueue(createWebserviceCallback(it))
        }
    }

    private fun insertItemsIntoDb(
        response: Response<RedditApi.ListingResponse>,
        it: PagingRequestHelper.Request.Callback) {
        ioExecutor.execute {
            handleResponse(subredditName, response.body())
            it.recordSuccess()
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: RedditPost) {
        // ignored, since we only ever append to what's in the DB
    }

    private fun createWebserviceCallback(it: PagingRequestHelper.Request.Callback)
            : Callback<RedditApi.ListingResponse> {
        return object : Callback<RedditApi.ListingResponse> {
            override fun onFailure(
                call: Call<RedditApi.ListingResponse>,
                t: Throwable) {
                it.recordFailure(t)
            }

            override fun onResponse(
                call: Call<RedditApi.ListingResponse>,
                response: Response<RedditApi.ListingResponse>
            ) {
                insertItemsIntoDb(response, it)
            }
        }
    }
}

