package pl.awalas.paging.presentation.byitem

import android.arch.lifecycle.ViewModel
import pl.awalas.paging.repository.RedditPostRepository

class SubRedditByItemViewModel(repository: RedditPostRepository) : ViewModel() {
    val posts = repository.postsOfSubreddit("androiddev", 30)

}

