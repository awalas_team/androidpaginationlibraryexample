package pl.awalas.paging.presentation.bypage

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import android.support.annotation.MainThread
import pl.awalas.paging.RedditPost
import pl.awalas.paging.api.RedditApi
import pl.awalas.paging.repository.RedditPostRepository
import java.util.concurrent.Executor

class SubRedditByPageRepository(
    private val redditApi: RedditApi,
    private val networkExecutor: Executor
) : RedditPostRepository {
    @MainThread
    override fun postsOfSubreddit(
        subReddit: String,
        pageSize: Int
    ): LiveData<PagedList<RedditPost>> {
        val sourceFactory = SubRedditByPageDataSourceFactory(redditApi, subReddit)

        return sourceFactory.toLiveData(
            pageSize = pageSize,
            fetchExecutor = networkExecutor
        )
    }
}