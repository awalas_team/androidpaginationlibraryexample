package pl.awalas.paging.presentation

import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_reddit.list
import org.koin.android.viewmodel.ext.android.viewModel
import pl.awalas.paging.R
import pl.awalas.paging.RedditPost
import pl.awalas.paging.presentation.byitem.SubRedditByItemViewModel
import pl.awalas.paging.presentation.bypage.SubRedditByPageViewModel
import pl.awalas.paging.presentation.byposition.SubRedditByPositionViewModel
import pl.awalas.paging.repository.RedditPostRepository

class RedditActivity : AppCompatActivity() {
    companion object {
        const val KEY_REPOSITORY_TYPE = "repository_type"
        fun intentFor(context: Context, type: RedditPostRepository.Type): Intent {
            val intent = Intent(context, RedditActivity::class.java)
            intent.putExtra(KEY_REPOSITORY_TYPE, type.ordinal)
            return intent
        }
    }

    val byPositionViewModel: SubRedditByPositionViewModel by viewModel()
    val byItemViewModel: SubRedditByItemViewModel by viewModel()
    val byPageViewModel: SubRedditByPageViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reddit)
        initAdapter()
        initSearch()
    }

    private fun initAdapter() {
        val adapter = PostsAdapter()
        list.adapter = adapter
        byPositionViewModel.posts.observe(this, Observer<PagedList<RedditPost>> {
            adapter.submitList(it)
        })
    }

    private fun initSearch() {
        list.scrollToPosition(0)
        (list.adapter as? PostsAdapter)?.submitList(null)
    }
}
