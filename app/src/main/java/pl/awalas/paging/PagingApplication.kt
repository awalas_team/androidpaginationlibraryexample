package pl.awalas.paging

import android.app.Application
import org.koin.android.ext.android.startKoin
import pl.awalas.paging.di.postsModule
import timber.log.Timber

class PagingRoomApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin(this, listOf(postsModule))
    }
}