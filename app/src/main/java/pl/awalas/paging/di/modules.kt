package pl.awalas.paging.di

import android.app.Application
import android.arch.persistence.room.Room
import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import pl.awalas.paging.api.RedditApi
import pl.awalas.paging.db.RedditDatabase
import pl.awalas.paging.presentation.byitem.SubRedditByItemViewModel
import pl.awalas.paging.presentation.bypage.SubRedditByPageViewModel
import pl.awalas.paging.presentation.byposition.SubRedditByPositionViewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val postsModule = module {

    viewModel { SubRedditByPositionViewModel(get()) }
    viewModel { SubRedditByItemViewModel(get()) }
    viewModel { SubRedditByPageViewModel(get()) }

    single { createDatabase(get()) }

    single { createPostsDao(get()) }

    single { createRedditApi() }

    single { SubR() }
    single { createRedditApi() }
    single { createRedditApi() }
}


fun createDatabase(application: Application) =
    Room.databaseBuilder(application, RedditDatabase::class.java, "RedditDatabase.db").build()

fun createPostsDao(database: RedditDatabase) = database.posts()

fun createRedditApi(): RedditApi {
    val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
        Log.d("API", it)
    })
    logger.level = HttpLoggingInterceptor.Level.BASIC

    val client = OkHttpClient.Builder()
        .addInterceptor(logger)
        .build()
    return Retrofit.Builder()
        .baseUrl("https://www.reddit.com/")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(RedditApi::class.java)
}