package pl.awalas.paging.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import pl.awalas.paging.RedditPost

interface RedditPostRepository {

    fun postsOfSubreddit(subReddit: String, pageSize: Int): LiveData<PagedList<RedditPost>>

    enum class Type {
        IN_MEMORY_BY_ITEM,
        IN_MEMORY_BY_PAGE,
        DB
    }
}